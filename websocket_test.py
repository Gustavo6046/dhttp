import trio
import threading
import random
import dhttp


server = None
port = random.randint(8000, 8999)
tests_done = 0
fails = False

def done():
    global tests_done
    tests_done += 1

    if tests_done == 2:
        print('Stopping...')
        server.stop()

        if fails:
            exit(1)

def success(name):
    print(' * SUCCESS: ' + name)
    done()

def failure(name):
    global fails
    fails = True

    print(' * FAILURE: ' + name)
    done()

async def run_server():
    global server 
    
    server = dhttp.DHTTPServer()
    server.add_port(port,
        tls = True,
        cert_mode = dhttp.tcp.CertMode.CM_GENERATED,
        cert_file = '.server.cert.pem',
        key_file = '.server.key.pem'
    )
    
    @server.websocket('/wstest')
    def handle_websocket(client, req):
        client._tries = 0

        @client.ws_receiver
        def receive_data(packet):
            print('server received: '.ljust(16), packet.content)

            if packet.content != b'AAA':
                client._tries += 1

                if client._tries < 10:
                    client.ws_write(packet.content + packet.content)

                else:
                    failure('WebSocket')

            else:
                success('WebSocket')

    @server.get('/cookietest_set')
    def cookie_test(req, res):
        res.set_cookie('Racecar', 'racecaR')
        res.end('Hi!')

    @server.get('/cookietest_get')
    def cookie_test(req, res):
        res.end(req.get_cookie('Racecar') or '')

    await server.run()

async def run_client():
    global server 

    while server is None:
        await trio.sleep(0.5)

    client = dhttp.DHTTPClient('127.0.0.1', port)

    await client.request('/cookietest_set', auto_start = True,
        tls = True,
        cert_mode = dhttp.tcp.CertMode.CM_GENERATED,
        cert_file = '.http_client.cert.pem',
        key_file = '.http_client.key.pem'
    )
    resp = await client.request('/cookietest_get', auto_start = True,
        tls = True,
        cert_mode = dhttp.tcp.CertMode.CM_GENERATED,
        cert_file = '.http_client.cert.pem',
        key_file = '.http_client.key.pem'
    )

    print('Client cookies:', repr(client.cookies))
    print('Response cookies:', repr(resp.content))

    if resp.content == b'racecaR':
        success('Cookies')

    else:
        failure('Cookies')

async def run_ws_client():
    global server 

    while server is None:
        await trio.sleep(0.5)

    ws = dhttp.DHTTPWebsocketClient('127.0.0.1', port)

    @ws.on_connect
    def _connected(client, _):
        buffer = dhttp.tcp.DequeBytesIO()

        @client.ws_receiver
        def got_answer(packet):
            print('client received: '.ljust(16), packet.content)
            
            if packet.content == b'DoubleDouble':
                client.ws_write('AAA')
                ws.close()
            
        client.ws_write('Double') 

    await ws.connect_run('/wstest',
        tls = True,
        cert_mode = dhttp.tcp.CertMode.CM_GENERATED,
        cert_file = '.ws.client.cert.pem',
        key_file = '.ws.client.key.pem'
    )

def run_server_thread():
    trio.run(run_server)

async def run():
    try:
        async with trio.open_nursery() as nursery:
            # -- Unfortunately, SSL can't run both sockets on the same thread. :(
            t1 = threading.Thread(name = 'server thread', target = run_server_thread)
            t1.start()

            nursery.start_soon(run_client)
            nursery.start_soon(run_ws_client)

    except KeyboardInterrupt:
        server.stop()
        exit(1)

def main():
    trio.run(run)

if __name__ == "__main__":
    main()